/*global $,document,window*/

function Warriors() {
    this.warrior_data = [];
    this.competitor = this.get_query_str(window.location.href);
}

Warriors.prototype.get_query_str = function(url) {
    var vars = [], hash, q = url.split('?')[1], i = 0;

    if (q !== undefined) {
        q = q.split('&');
        for (i; i < q.length; i++) {
            hash = q[i].split('=');
            vars[hash[0]] = hash[1];
        }
    }
    return vars;
};

Warriors.prototype.init = function() {
    var i = 0, lower, position, $this = this;

    $.ajax({
        url: 'http://www.army.mil/bestwarrior/warriors/warrior.json',
        type: 'get',
        dataType: 'json',
        success: function(data) {
            // position = competitor.pos ? parseInt(competitor.pos, 10) : 0;

            if (data) {
                $this.warrior_data = data.slice(0);
                for (i; i < data.length; i++) {
                    lower = data[i].name.toLowerCase();
                    if (lower.match($this.competitor.first) &&
                        lower.match($this.competitor.last))
                        position = i;
                }
                if (!position) position = 0;
                $this.build_warrior_info(position);
            }
        },

        error: function(jqXHR, textStatus) {
            $('.top-warrior').prepend(
                '<p>Error retrieving warrior data: ' + textStatus + '</p>'
            );

            $('<img>').load(function() {
                $('#portrait').append($(this)).css('border', 'none');
            }).prop({
                'src': 'http://usarmy.vo.llnwd.net/e2/rv5_images/' +
                    'bestwarrior/graphics/bwc_shield.png',
                'alt': 'Error retrieving warrior photo'
            });
        }
    });
};

Warriors.prototype.build_warrior_info = function(position) {
    var i = 0, $this = this;

    $('<img>').load(function() {
        $('#portrait').append($(this));
    }).prop({
        'src': $this.warrior_data[position].image,
        'alt': $this.warrior_data[position].name
    });

    $('#warrior_name').html($this.warrior_data[position].name);

    $('#occupation').html($this.warrior_data[position].mos);

    $('#assigned_unit').html($this.warrior_data[position].current);

    $('#hometown').html($this.warrior_data[position].hometown);

    $('#years').html($this.warrior_data[position].years);

    for (i; i < $this.warrior_data[position].interview.length; i++) {
        $('div#interview').append(
            '<div class="question_answer">' +
            '<p class="question">' +
                $this.warrior_data[position].interview[i].question +
            '</p><p class="answer">' +
                $this.warrior_data[position].interview[i].answer +
            '</p></div>'
        );
    }

    $('div.question_answer:gt(2)').hide();

    $('#more').click(function() {
        $(this).hide();
        $('.question_answer').show();
        $('#less').show();
    });

    $('#less').click(function() {
        $(this).hide();
        $('.question_answer:gt(2)').hide();
        $('#more').show();
    });

    $('#video-wrap').append(
        '<iframe width="725" height="408" ' +
        'src="//www.youtube-nocookie.com/embed/videoseries?list=' +
        $this.warrior_data[position].video +
        '&rel=0" frameborder="0" allowfullscreen></iframe>'
    );
};

$(document).ready(function() {
    var warriors = new Warriors();

    /*
     * jshint kept complaining about unused warriors var
     * so i put the init call here instead of inside Warriors function
     */
    warriors.init();
});
